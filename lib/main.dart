import 'dart:convert';
import 'dart:io';

import 'package:bill_ease/auth/login.dart';
import 'package:bill_ease/misc/navigator.dart';
import 'package:bill_ease/services/supabase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await rootBundle
      .loadString('assets/text/secrets.json')
      .then((final String file) async {
    final Map<String, dynamic> secrets =
        json.decode(file) as Map<String, dynamic>;

    String supabaseUrl = secrets['local_url'];

    if (kDebugMode && Platform.isAndroid) {
      supabaseUrl = secrets['local_url_android'];
    } else if (!kDebugMode) {
      supabaseUrl = secrets['url'];
    }
    final String anonKey = kDebugMode
        ? secrets['local_anon-public-key']
        : secrets['anon-public-key'];

    await Supabase.initialize(
      url: supabaseUrl,
      anonKey: anonKey,
    ).then((final _) {
      authStateListener();
    });
  });

  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(final BuildContext context) => MaterialApp(
        home: const LoginScreen(),
        navigatorKey: globalNav,
      );
}
