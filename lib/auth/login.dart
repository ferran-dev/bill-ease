import 'package:bill_ease/auth/register.dart';
import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String? _email;
  String? _password;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(final BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Login to Bill-ease'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                const Expanded(
                  child: FlutterLogo(),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 40),
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        decoration: const InputDecoration(
                          labelText: 'Email',
                        ),
                        keyboardType: TextInputType.emailAddress,
                        validator: (final String? value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter an email';
                          }
                          _email = value;
                          return null;
                        },
                      ),
                      const SizedBox(height: 16),
                      TextFormField(
                        decoration: const InputDecoration(
                          labelText: 'Password',
                        ),
                        obscureText: true,
                        validator: (final String? value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a password';
                          }
                          _password = value;
                          return null;
                        },
                      ),
                      const SizedBox(height: 24),
                      OutlinedButton(
                        child: const Text('LOGIN'),
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            final SupabaseClient supabase =
                                Supabase.instance.client;

                            await supabase.auth.signInWithPassword(
                              email: _email,
                              password: _password!,
                            );
                          }
                        },
                      ),
                      TextButton(
                        child: const Text('Sign up'),
                        onPressed: () async {
                          await Navigator.of(context).push(
                            MaterialPageRoute<SignUpScreen>(
                              builder: (final BuildContext context) =>
                                  const SignUpScreen(),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
