import 'package:bill_ease/auth/login.dart';
import 'package:bill_ease/misc/contants.dart';
import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({super.key});

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  String? _email;
  String? _password;
  String? _confirmPassword;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(final BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Sign Up for Bill-ease'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                const FlutterLogo(size: 150),
                const SizedBox(height: 48),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Email',
                  ),
                  keyboardType: TextInputType.emailAddress,
                  validator: (final String? value) {
                    if (value == null ||
                        value.isEmpty ||
                        !emailFormat.hasMatch(value)) {
                      return 'Please enter a valid email';
                    }
                    _email = value;
                    return null;
                  },
                ),
                const SizedBox(height: 16),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Password',
                  ),
                  obscureText: true,
                  validator: (final String? value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter a password';
                    } else if (value.length < 8) {
                      return 'Password must be at least 8 characters long';
                    }
                    _password = value;
                    return null;
                  },
                ),
                const SizedBox(height: 24),
                OutlinedButton(
                  child: const Text('SIGN UP'),
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('Processing Data')),
                      );

                      final SupabaseClient supabase = Supabase.instance.client;

                      final AuthResponse res = await supabase.auth.signUp(
                        email: _email!,
                        password: _password!,
                      );
                      final Session? session = res.session;
                      final User? user = res.user;
                    }
                  },
                ),
                TextButton(
                  child: const Text('Already have an account? Login'),
                  onPressed: () async {
                    await Navigator.of(context).push(
                      MaterialPageRoute<LoginScreen>(
                        builder: (final BuildContext context) =>
                            const LoginScreen(),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      );
}
