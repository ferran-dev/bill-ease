// A screen that allows users to take a picture using a given camera.
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';

class CameraView extends StatefulWidget {
  const CameraView({
    super.key,
  });

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<CameraView> {
  CameraController? controller;
  late List<CameraDescription> _cameras;

  Future<void> _getCameras() async {
    _cameras = await availableCameras();
  }

  @override
  void initState() {
    super.initState();
    _getCameras().then((value) {
      controller = CameraController(_cameras[0], ResolutionPreset.max);
      controller!.initialize().then((final _) {
        if (!mounted) {
          return;
        }
        setState(() {});
      }).catchError((final Object e) {
        if (e is CameraException) {
          switch (e.code) {
            case 'CameraAccessDenied':
              // Handle access errors here.
              break;
            default:
              // Handle other errors here.
              break;
          }
        }
      });
    });
  }

  @override
  Widget build(final BuildContext context) {
    if (controller == null || controller!.value.isInitialized) {
      return Container();
    }
    return MaterialApp(
      home: CameraPreview(controller!),
    );
  }
}
