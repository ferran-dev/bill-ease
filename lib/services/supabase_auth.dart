import 'package:bill_ease/auth/login.dart';
import 'package:bill_ease/camera_view.dart';
import 'package:bill_ease/misc/navigator.dart';
import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

void authStateListener() {
  final SupabaseClient supabase = Supabase.instance.client;

  supabase.auth.onAuthStateChange.listen((final AuthState event) async {
    if (globalNav.currentState == null) {
      return;
    }
    if (event.session == null) {
      await globalNav.currentState!.push(
        MaterialPageRoute<LoginScreen>(
          builder: (final BuildContext context) => const LoginScreen(),
        ),
      );
    } else {
      await globalNav.currentState!.push(
        MaterialPageRoute<CameraView>(
          builder: (final BuildContext context) => CameraView(),
        ),
      );
    }
  });
}
