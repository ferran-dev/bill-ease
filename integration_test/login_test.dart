import 'package:bill_ease/main.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  testWidgets('Login flow', (final WidgetTester tester) async {
    await tester.pumpWidget(const MainApp());
    expect(find.text('Login to Bill-ease'), findsOneWidget);
  });
}
